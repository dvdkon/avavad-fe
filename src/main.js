import Vue from "vue";
import VueRouter from "vue-router";
import HomePage from "./components/HomePage.vue";
import AvavaMain from "./components/AvavaMain.vue";

Vue.use(VueRouter);

const routes = [
    {path: "/", component: HomePage},
];

const router = new VueRouter({
    routes,
});

document.addEventListener("DOMContentLoaded", () => {
    let vueMain = new Vue({
        router,
        render: h => h(AvavaMain),
    }).$mount("body");
});
