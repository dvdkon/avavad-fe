import config from "./config.js";

// "Ugly global" that makes everything considerably simpler
// Would be lovely to have it "application-wide", but that's what a global
// variable is in a web browser.
let AUTH_TOKEN;

class EndpointCallException {
    constructor(status, respJson) {
        this.status = status;
        this.respJson = respJson;
    }

    toString() {
        return `Calling endpoint returned error code: ${res.status}`;
    }
}

async function callEndpoint(method, name, args = {}, authToken = AUTH_TOKEN)  {
    let url = config.apiUrl + "/" + name;
    if(method === "GET") {
        const params = new URLSearchParams(args);
        url += "?" + params.toString();
    }
    let body;
    if(method === "POST") {
        const form = new FormData();
        for(const key of Object.keys(args)) {
            form.append(key, args[key]);
        }
        body = form.toString();
    }
    const res = await fetch(url, {
            method,
            body,
            headers: {
                "X-Avavad-Auth": authToken,
            },
        });
    let jsonResp;
    try {
        jsonResp = await res.json();
    } catch { }
    if(!res.ok) {
        throw new EndpointCallException(res.status, jsonResp);
    }
    return jsonResp;
}

async function loginWithPassword(username, password) {
    let resp = await callEndpoint("POST", "/pw-auth-login", {
            username, password
        });
    AUTH_TOKEN = resp.token;
}
