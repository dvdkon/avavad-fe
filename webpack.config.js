const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
    mode: "development",
    entry: "./src/main.js",
    plugins: [
        new VueLoaderPlugin(),
    ],
    module: {
        rules: [
            {test: /\.vue$/, loader: "vue-loader"},
            {test: /\.js$/, use: {loader: "babel-loader", options: {
                presets: ["@babel/preset-env"],
            }}},
            {test: /\.css$/, use: ["vue-style-loader", "css-loader"]},
            {test: /\.pug$/, loader: "pug-plain-loader"},
        ],
    },
}
